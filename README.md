# OpenML dataset: Quality-Prediction-in-a-Mining-Process

https://www.openml.org/d/43311

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
The dataset comes from one of the most important parts of a mining process: a flotation plant
The main goal is to use this data to predict how much impurity is in the ore concentrate. As this impurity is measured every hour, if we can predict how much silica (impurity) is in the ore concentrate, we can help the engineers, giving them early information to take actions (empowering). Hence, they will be able to take corrective actions in advance (reduce impurity, if it is the case) and also help the environment (reducing the amount of ore that goes to tailings as you reduce silica in the ore concentrate).
Content
The first column shows time and date range (from march of 2017 until september of 2017). Some columns were sampled every 20 second. Others were sampled on a hourly base. 
The second and third columns are quality measures of the iron ore pulp right before it is fed into the flotation plant. Column 4 until column 8 are the most important variables that impact in the ore quality in the end of the process. From column 9 until column 22, we can see process data (level and air flow inside the flotation columns, which also impact in ore quality. The last two columns are the final iron ore pulp quality measurement from the lab. 
Target is to predict the last column, which is the % of silica in the iron ore concentrate.
Inspiration
I have been working in this dataset for at least six months and would like to see if the community can help to answer the following questions:
Is it possible to predict % Silica Concentrate every minute 
How many steps (hours) ahead can we predict % Silica in Concentrate This would help engineers to act in predictive and optimized way, mitigatin the % of iron that could have gone to tailings.
Is it possible to predict % Silica in Concentrate whitout using % Iron Concentrate column (as they are highly correlated)

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43311) of an [OpenML dataset](https://www.openml.org/d/43311). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43311/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43311/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43311/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

